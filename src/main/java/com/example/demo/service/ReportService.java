package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {


	@Autowired
	ReportRepository reportRepository;

	//投稿関連
	public List<Report> findAllReport(){
		return reportRepository.findAll(Sort.by(Sort.Direction.DESC,"id"));
	}

	public Report findAReport(int id) {
		return reportRepository.getById(id);
	}

	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	public void deleteReport(int id) {
		reportRepository.deleteById(id);
	}

	public void upDateReport(Report report) {
		reportRepository.save(report);
	}

	public List<Report> sarchForDate(String startDate , String endDate) {
		Date start = null;
		Date end = null;
		if(startDate != null && !startDate.isEmpty()) {
			startDate += " 00:00:00";
		}else {
			startDate = "2020-01-01 00:00:00";
		}

		if(endDate != null && !endDate.isEmpty()) {
			endDate+= " 23:59:59";
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			start = sdf.parse(startDate);
			if(endDate == null|| endDate.isEmpty()) {
				end =  new Date();
			}else {
				end = sdf.parse(endDate);
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return reportRepository.findAll();
		}
		return reportRepository.findByCreatedDateBetweenOrderByUpdatedDateDesc(start, end);
	}
}
