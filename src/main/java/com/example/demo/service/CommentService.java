package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {

	@Autowired
	CommentRepository commentRepository;

	//コメント返信関連
		public void saveComment(Comment comment) {
			commentRepository.save(comment);
		}

		public List<Comment> findAll(Integer id) {
			List<Comment> searchForId = new ArrayList<>();
			List<Comment> listAll = commentRepository.findAll(Sort.by(Sort.Direction.DESC,"updatedDate"));
			for(Comment list:listAll) {
				if(list.getContentId() == id) {
					searchForId.add(list);
				}
			}
			return searchForId;
		}

		public Comment findACommnet(Integer id) {
			return commentRepository.getById(id);
		}

		public void upDateComment(Comment comment) {
			commentRepository.save(comment);
		}

		public void deleteComment(Integer id) {
			commentRepository.deleteById(id);
		}
}
