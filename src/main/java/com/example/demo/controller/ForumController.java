package com.example.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;
	//トップページ表示
	@GetMapping
	public ModelAndView top(@RequestParam(required=false) String startDate , @RequestParam(required=false) String endDate) {
		ModelAndView mav = new ModelAndView();
		List<Report> contentData = reportService.sarchForDate(startDate, endDate);
		mav.setViewName("/top");
		mav.addObject("contents", contentData);
		mav.addObject("startDate",startDate);
		mav.addObject("endDate",endDate);
		return mav;
	}

	//新規投稿画面への遷移
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	//投稿の編集画面への遷移
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") int id) {
		Report contentData = reportService.findAReport(id);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/edit");
		mav.addObject("contentData",contentData );
		return mav;
	}

	//投稿及び、紐づいたコメントの操作画面への遷移
	@GetMapping("/view/{id}")
	public ModelAndView viewOneReportAndComMent(@PathVariable("id") int id) {
		Report contentData = reportService.findAReport(id);
		List<Comment> commentList = commentService.findAll(id);
		ModelAndView mav = new ModelAndView();
		Comment commentData = new Comment();
		mav.setViewName("/view");
		mav.addObject("commentList", commentList);
		mav.addObject("contentData",contentData );
		mav.addObject("commentData", commentData);
		return mav;
	}

	//コメントの編集画面への遷移
	@GetMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable("id") int id) {
		Comment commentData = commentService.findACommnet(id);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/comment");
		mav.addObject("commentData", commentData);
		return mav;
	}

	//投稿の追加
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		reportService.saveReport(report);

		return new ModelAndView("redirect:/");
	}

	//コメントの追加
	@PostMapping("/comment")
	public ModelAndView addComment(@ModelAttribute("commentData") Comment comment) {
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/view/"+ comment.getContentId());
	}

	//投稿の編集
	@PutMapping("/edit")
	public ModelAndView edit(@ModelAttribute("contentData") Report report) {
		report.setUpdatedDate(new Date());
		reportService.upDateReport(report);
		return new ModelAndView("redirect:/");
	}

	//コメントの編集
	@PutMapping("/editComment")
	public ModelAndView editComment(@ModelAttribute("commentData")Comment comment) {
		comment.setUpdatedDate(new Date());
		commentService.upDateComment(comment);
		return new ModelAndView("redirect:/");
	}

	//投稿の削除
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable("id") Integer id) {

		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable("id") Integer id , @RequestParam("id") int userId) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/view/"+userId);
	}
}
